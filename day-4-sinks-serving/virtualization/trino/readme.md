```shell
https://trino.io/docs/current/connector/kafka.html

NAMESPACE=processing
TRINO=`kubectl get pods -n $NAMESPACE -l app.kubernetes.io/component=coordinator -o name | cut -d'/' -f 2`
kubectl exec $TRINO -it -- /usr/bin/trino --debug

http://localhost:9000/ui/

jdbc:trino://localhost:8080
trino:***empty***
```

```sql
-- view metadata info
SHOW SCHEMAS FROM kafka;
SHOW TABLES FROM kafka.default;

-- describe data
DESCRIBE kafka.default."src-app-agent-json";
DESCRIBE kafka.default."src-app-users-json";

-- count rows
SELECT COUNT(*) FROM kafka.default."src-app-agent-json";
SELECT COUNT(*) FROM kafka.default."src-app-users-json";

-- read data from topics
SELECT * FROM kafka.default."src-app-agent-json" LIMIT 10;
SELECT * FROM kafka.default."src-app-users-json" LIMIT 10;

-- retrieve message (payload)
SELECT _message FROM kafka.default."src-app-agent-json" LIMIT 10;
SELECT _message FROM kafka.default."src-app-users-json" LIMIT 10;

-- cast message to retrieve column
-- using fn - json_extract_scalar
SELECT cast(json_extract_scalar(_message, '$.user_id') AS varchar) AS user_id
FROM kafka.default."src-app-agent-json" LIMIT 10;

-- retrieve music data
-- group data and order by 
SELECT cast(json_extract_scalar(_message, '$.genre') AS varchar) AS user_id, COUNT(*) AS Q
FROM kafka.default."src-app-musics-json"
GROUP BY cast(json_extract_scalar(_message, '$.genre') AS varchar)
ORDER BY Q DESC
LIMIT 3;

/*
{
    "user_id": 582,
    "uuid": "8447e3ad-2ec4-43fb-a71c-e2693110c67d",
    "first_name": "Robert",
    "last_name": "Fowler",
    "date_birth": "1999-09-15",
    "city": "Lake Jason",
    "country": "Marshall Islands",
    "company_name": "Garner-Jones",
    "job": "Surveyor, hydrographic",
    "phone_number": "(384)978-0671x4523",
    "last_access_time": "1996-06-04T03:33:07",
    "time_zone": "Africa/Algiers",
    "dt_current_timestamp": "2021-07-28 10:54:26.389474"
}
*/

-- postgres metadata
SHOW TABLES FROM postgres.public;

-- read table from postgres
-- yugabytedb
SELECT *
FROM postgres.public.device

-- select another table
SELECT *
FROM postgres.public.ksqldb_enriched_pr_musics_analysis
LIMIT 10;

-- group data 
SELECT country,
       music_genre AS genre,
       COUNT(*) AS amount
FROM postgres.public.ksqldb_enriched_pr_musics_analysis
GROUP BY country, music_genre
ORDER BY amount DESC;

-- join between kafka and postgres schemas
SELECT cast(json_extract_scalar(users._message, '$.city') AS varchar) AS city,
       cast(json_extract_scalar(users._message, '$.country') AS varchar) AS country,
       device.model,
       device.dt_current_timestamp
FROM kafka.default."src-app-users-json" AS users
INNER JOIN postgres.public.device AS device
ON cast(json_extract_scalar(users._message, '$.user_id') AS int) = device.user_id
LIMIT 10;

-- group data using country column
SELECT cast(json_extract_scalar(users._message, '$.country') AS varchar) AS country,
       device.model,
       COUNT(*) AS amount
FROM kafka.default."src-app-users-json" AS users
INNER JOIN postgres.public.device AS device
ON cast(json_extract_scalar(users._message, '$.user_id') AS int) = device.user_id
GROUP BY cast(json_extract_scalar(users._message, '$.country') AS varchar), device.model
ORDER BY amount DESC
LIMIT 10;

-- show elastic data
SHOW TABLES FROM elk.default;

-- query data
SELECT *
FROM elk.default."src-app-movies-titles-json"
LIMIT 10;
```