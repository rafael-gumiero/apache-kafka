```shell
# cluster 
kubectx aks-owshq-orion

# access namespace
kubens viz

# resources
superset-588d8b4f98-2fxbh          1/1     Running     0          18d
superset-init-db-rsnbn             0/1     Completed   0          9d
superset-postgresql-0              1/1     Running     0          18d
superset-redis-master-0            1/1     Running     0          18d
superset-worker-85d6d76c6f-ksdzb   1/1     Running     0          18d

# verify services
k get svc
http://localhost:8088/login/
user & pwd = admin

# create 
database
datasets
sqllab
charts
dashboard
```