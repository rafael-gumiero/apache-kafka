# Python MongoDB Producer
> the python app produce data to a mongodb database
> using incremental load (cdc)

## local environment [venv] setup
> this is a manual that shows how to set up a virtual environment 
> to install the requested libraries to run the program

````sh
# install pip
pip3 install virtualenv

# check version
virtualenv --version

# path where program resides
/Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data

# create virtual env
virtualenv venv  

# activate env
source venv/bin/activate

# install requisites  
pip install -r requirements.txt  

# execute program
python3.8 app.py
curl -i http://localhost:7900/mongodb?document=10

# leave virtual environment
deactivate  

# delete env if necessary
rm -rf venv
````  

### step 1 - dockerize app
> create a dockerfile and containerize application

````sh
# list local images
docker images

# navigate to local
/Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data
Dockerfile

# build app
docker build --tag python-producer-mongodb-app .

# open container
docker run -i -t python-producer-mongodb-app /bin/bash

# run app
docker run python-producer-mongodb-app
docker run python-producer-mongodb-app python3.7 main.py

# remove dangling images
docker rmi $(docker images --filter "dangling=true" -q --no-trunc) -f
````

### step 2 - docker hub [registry]
> upload code written into a container registry - docker hub

```sh
# login
docker login

# docker hub
https://hub.docker.com/repositories

# tag image
docker tag python-producer-mongodb-app owshq/python-producer-mongodb-app:2.0

# images
docker images
owshq/python-producer-mongodb-app:2.0

# push image to registry
docker push owshq/python-producer-mongodb-app:2.0
```

### step 3 - deploy application on kubernetes [k8s]
> deploying app into kubernetes cluster

```sh
# location
/Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data/deployment/deployment.yaml

# deploy app
k apply -f /Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data/deployment/deployment.yaml -n app

# deployment info
k describe deployment python-producer-user-data

# list & describe pods
k get pods -l name=app-user-data-flaskapi
k describe pod -l name=app-user-data-flaskapi

# verify logs
k logs -l name=app-user-data-flaskapi -f

# remove deployment
k delete deployment python-producer-user-data
```

### step 4 - test app deployment
> log into container and test, deploy service to expose app

```sh
# access container
k exec -c app-user-data-flaskapi -it python-producer-user-data-554bcb7945-pbtdd  -- /bin/bash

# service
/Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data/deployment/service.yaml

# deploy service
k apply -f /Users/luanmorenomaciel/BitBucket/apache-kafka/5-connect-source-api/apps/user-data/deployment/service.yaml

# describe service
k describe service app-python-producer-user-data-aks-lb

# remove service
k delete service app-python-producer-user-data-aks-lb
```

### step 5 - send data to mongodb [test]
> test application using http requests

```sh
# describe load balancer
k describe svc app-python-producer-user-data-aks-lb
52.167.77.110

# flask http requests [mongodb]
http://52.167.77.110/mongodb?document=15

# curl requests
curl -i http://52.167.77.110/mongodb?document=16
```