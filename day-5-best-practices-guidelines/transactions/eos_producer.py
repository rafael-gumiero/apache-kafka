# import libraries
import json
import delivery_reports
import producer_settings
from random import randint
from confluent_kafka import Producer, KafkaException
from users import Users


# producer class that generate events
class EOSKafkaProducer(object):

    # produce events using eos configuration
    @staticmethod
    def json_eos_producer(broker, kafka_topic, get_dt_rows):

        # init producer settings
        p = Producer(producer_settings.producer_settings_json(broker))

        # initialize producer transaction
        # blocking call that will require producer id from the
        # register transactional.id with the transaction coordinator [inside of kafka broker]
        # close any pending transactions and bumps epoch to fence out zombies
        p.init_transactions()

        # get object [dict] from objects
        # transform into tuple to a multiple insert process
        get_data = Users().get_multiple_rows(get_dt_rows)
        print(get_data)

        # for loop to insert events
        for data in get_data:

            try:
                # start producer transaction
                # instance may only have one single on-going transaction at a time
                # events are gonna either be committed or aborted atomically (acid)
                # not allowed to produce messages outside a transaction boundary
                p.begin_transaction()

                # trigger any available delivery report callbacks
                # from previous produce() calls
                p.poll(0)

                # produce events to the kafka topic
                # using json serializer
                # calling delivery reports (callback)
                # writes the state inside of the transaction log topic
                p.produce(
                    topic=kafka_topic,
                    key=str(randint(0, 100)),
                    value=json.dumps(data).encode('utf-8'),
                    callback=delivery_reports.on_delivery_json)

                # complete transaction operation
                # success or failure outcome
                # a = atomicity property of acid
                # request is sent to the coordinator to being the two-phase commit protocol
                p.abort_transaction()
                # p.commit_transaction(10.0)
                break

            # catch exception generated by the program
            # failure can be traced
            except KafkaException as e:
                continue

        # abort transaction if something happens
        # acid property being executed ~ failed cases
        # request is sent to the coordinator to being the two-phase commit protocol
        else:
            p.abort_transaction()

        # wait for any outstanding messages
        # to be delivered and delivery report
        p.flush()
