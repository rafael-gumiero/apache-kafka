# import libraries
import os
from dotenv import load_dotenv

# get env
load_dotenv()

# load variables
kafka_bootstrap_server = os.getenv("KAFKA_BOOTSTRAP_SERVER")
kafka_client_id = os.getenv("KAFKA_CLIENT_ID")
kafka_transactional_id = os.getenv("KAFKA_TRANSACTIONAL_ID")


# [json] = producer config
def producer_settings_json(broker):

    # client.id = application identifier
    # bootstrap = kafka brokers
    # acks = broker will block until message is committed by all in-sync replicas (leader and followers)
    # enable.idempotence = events produced in eos and in order
    # max.in.flight.requests.per.connection = in-flight connections per broker connection
    # retries = times to retry an event
    # transactional.id = fence stale transactions from previous instances ~ outage or crash
    json = {
        "client.id": kafka_client_id,
        "bootstrap.servers": broker,
        "acks": "all",
        "enable.idempotence": "true",
        "max.in.flight.requests.per.connection": 5,
        "retries": 10,
        "transactional.id": kafka_transactional_id
        }

    # return data
    return dict(json)