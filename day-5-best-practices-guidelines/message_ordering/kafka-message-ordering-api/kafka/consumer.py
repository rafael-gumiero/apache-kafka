#import libs

import json
import pandas as pd
from confluent_kafka.cimpl import KafkaException, Consumer
import consumer_settings
from dotenv import load_dotenv
import datetime
import time
import logging
import os

# get env
load_dotenv()

# load variable
kafka_topic = os.getenv("KAFKA_TOPIC_MESSSAGE_ORDERING_JSON")
execution_time = os.getenv("EXECUTION_TIME")


# instance configuration for kafka consumer
c = Consumer(consumer_settings.consumer_settings_json())

# subscribe the topic be consume
c.subscribe([kafka_topic])

# duration of application execution
timeout = time.time() + int(execution_time)

try:
    while timeout >= time.time():
        events = c.poll(0.1)
        if events is None:
            continue
        if events.error():
            raise KafkaException(events.error())
        print(events.topic(), events.partition(), events.offset(), events.value().decode('utf-8'))

except KeyboardInterrupt:
    pass

finally:
    c.close()