python-dotenv==0.16.0
pandas==1.2.3
numpy==1.20.2
confluent-kafka==1.7.0
confluent_avro==1.8.0
avro-python3==1.10.2